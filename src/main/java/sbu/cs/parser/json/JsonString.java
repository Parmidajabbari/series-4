package sbu.cs.parser.json;

public class JsonString {
    private String key;
    private String value;
    public void setkey(String key) {
        this.key = key;
    }
    public void setvalue(String value) {
        this.value = value;
    }
    public String getkey() {
        return key;
    }
    public String getvalue() {
        return value;
    }
}
