package sbu.cs.parser.json;

import java.util.ArrayList;

public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
        // TODO implement this
        if(data.contains("[")) {
            StringBuilder string = new StringBuilder(data);
            for(int i = string.indexOf("[") ; i < string.indexOf("]") ; i++) {
                if(string.charAt(i) == ',') {
                    string.setCharAt(i, '-');
                }
                if(string.charAt(i) == ' ') {
                    string.setCharAt(i, '%');
                }
            }
            data = string.toString();
        }
        data = data.replaceAll("(}|\\{|\\s|\")" , "");
        String[] split1 = data.split(",");
        ArrayList<JsonString> temp = new ArrayList<>();
        for(int i = 0 ; i < split1.length ; i++) {
            split1[i] = split1[i].replaceAll("-" , ",");
            split1[i] = split1[i].replaceAll("%", " ");
            String[] split2 = split1[i].split(":");
            JsonString temp2 = new JsonString();
            temp2.setkey(split2[0]);
            temp2.setvalue(split2[1]);
            temp.add(temp2);
        }
        Json parse = new Json();
        parse.setData(temp);
        return parse;
    }

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
