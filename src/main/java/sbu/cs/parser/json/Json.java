package sbu.cs.parser.json;
import java.util.ArrayList;
public class Json implements JsonInterface {

    private ArrayList<JsonString> data = new ArrayList<>();
    public void setData(ArrayList<JsonString> data) {
        this.data = data;
    }

    public String getStringValue(String key) {
        String temp = "";
        for(int i = 0 ; i < data.size() ; i++) {
            JsonString check;
            check = data.get(i);
            if(check.getkey().equals(key)) {
                temp =check.getvalue();
            }
        }
        return temp;
    }
}
